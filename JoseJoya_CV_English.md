<div class="row">
    <div class="flex" style="padding: 2rem 2rem 1rem 2rem;">
        <img src="./images/Avatar.png" width="120" class="cover-image" />
        <h3 class="contact-info">CONTACT INFO</h3>
        <p class="text-left underline">Bogotá, Colombia</p>
        <p class="text-left underline">ing.jose.joya@gmail.com</p>
        <p class="text-left underline">+57 3165385057</p>
        <a class="text-left block" href="https://seobryn.com">https://seobryn.com</a>
        <span class="underline mt-2"></span>
        <h3 class="contact-info mt-1">TECHNICAL SKILL</h3>
        <div class="text-left row space-between my-1">
            <span>HTML</span>
            <span class="progress-bar">
                <span class="progress percent-80"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>CSS</span>
            <span class="progress-bar">
                <span class="progress percent-80"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>Sass</span>
            <span class="progress-bar">
                <span class="progress percent-70"></span>
            </span>
        </div>
        <div class="text-left row space-between my-1">
            <span>JS</span>
            <span class="progress-bar">
                <span class="progress percent-90"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>Vue.js</span>
            <span class="progress-bar">
                <span class="progress percent-90"></span>
            </span>
        </div>
        <div class="text-left row space-between my-1">
            <span>React.js</span>
            <span class="progress-bar">
                <span class="progress percent-60"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>Unity3D</span>
            <span class="progress-bar">
                <span class="progress percent-80"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>Flutter</span>
            <span class="progress-bar">
                <span class="progress percent-80"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>Ionic 1/2</span>
            <span class="progress-bar">
                <span class="progress percent-70"></span>
            </span>
        </div>
        <div class="text-left my-1 row space-between">
            <span>NativeScript</span>
            <span class="progress-bar">
                <span class="progress percent-70"></span>
            </span>
        </div>
        <span class="underline"></span>
        <h3 class="contact-info mt-1">LANGUAGE</h3>
        <div class="text-left my-1">
            <span>Spanish</span>
            <span class="slider-bar">
                <span class="slider percent-100-slide"></span>
            </span>
        </div>
        <div class="text-left my-1">
            <span>English</span>
            <span class="slider-bar">
                <span class="slider percent-70-slide"></span>
            </span>
        </div>
        <span class="underline mt-6 "></span>
        <h3 class="contact-info mt-1">REFERENCE</h3>
        <p class="mt-2 text-left">
            Diego Padilla<br />
            Consultant and FullStack Developer<br />
            PwC<br />
            Email: diegoarpar@gmail.com <br />
            Phone: +57 3176653882
        </p>
        <p class="mt-2 text-left">
            Juan Manuel Cortés<br />
            CRO<br />
            Modyo<br />
            Email: juanmacortes@gmail.com <br />
            Phone: +57 3005445057
        </p>
    </div>
    <div class="flex white" style="padding: 1rem 1.5rem;">
        <h1 class="zero-margin text-left">JOSE JOYA</h1>
        <h3 class="zero-margin text-left" style="color:#879096; font-weight: 900;">
            Frontend Developer & Chapter Lead
        </h3>
        <h3 class="body-headline my-2">Profile Summary</h3>
        <p>
            Systems and informatic engineering experienced in software development and video game development, with
            great teamwork skills and more than 8 years working with web technologies like Vuejs, Angular 1.x and 2.x,
            Ionic framework, Nativescript and Flutter, and also 3 years experienced working with mobile native
            technologies like Android and IOS. I like to work with responsibility and clear goals that the organization
            set me. I have experience working with Agile Methodologies and GIT version management tech. I alway want to
            mark the difference in the things that I have to do.
        </p>
        <h3 class="body-headline my-2">Education</h3>
        <p>
            <b>Bachelor of Systems and Informatic Engineering</b> Universidad Pontificia Bolivariana (2010/2016)
        </p>
        <ul style="margin: 0; padding:0 0 0 3rem;" class="text-left">
            <li>final Grade: 4.5/5</li>
        </ul>
        <h3 class="body-headline my-2">Work experience</h3>
        <ul style="margin: 0; padding:0 0 0 3rem;">
            <li>
                <b>Strategy Consultant</b> / IBM de Colombia / 2015-12 <br />
                <i>Proyecto de transformación tecnológica SAP Worked as strategic consultant, supporting the
                    transformation process in Center of Competence SAP to Claro Colombia.</i>
            </li>
            <li class="mt-2">
                <b>Mobile developer</b> / IBM de Colombia / 2017-12 <br />
                <i>Hybrid mobile development for Seguros Bolivar Worked as a Mobile developer in AngularJS, and I
                    also worked as technical lead, contributing developing mobile hybrid app (IOS/ANDROID) with Ionic
                    framework 1.x to manage healthcare services.</i>
            </li>
            <li class="mt-2">
                <b>S.S. Angular.js Dev</b> / IBM de Colombia / 2018-03 <br />
                <i>Develop of UI Toolkit and BPM Flows at Grupo AVAL.
                    Develop an ui toolkit to handle all visual controls in the app, and also working implementing BPM
                    Flows in IBM BPM 8.5.7 and making integrations with legacy services.</i>
            </li>
            <li class="mt-2">
                <b>Mobile Developer</b> / Cecropia Solutions / 2019-05 <br />
                <i>Development hybrid mobile applications with nativescript with angular to reliqhealth technologies,
                    added support for IOS and Android, with integrations with backend API’s and Biometric devices,
                    supporting video calls and chat. supporting visual styles in the app.</i>
            </li>
            <li class="mt-6">
                <b>Frontend lead</b> / Modyo / 2020-05 <br />
                <i>Developing web applications and mobile web with Javascript, VueJS, SASS and also working generating
                    development standards for all frontend team in the company.</i>
            </li>
            <li class="mt-2">
                <b>S.S Web Developer</b> / Munitienda / 2020-08<br />
                <i>Developing web applications and mobile web with Javascript, ReactJS, CSS and contributing in some
                    projects with different technologies like Typescript, PHP, Python desktop, Flutter, Electron,
                    Javascript, Firebase, AWS.</i>
            </li>
            <li class="mt-2">
                <b>Software Designer & Chapter Lead</b> / Globant / Current<br />
                <i>
                <ul class="inner-list">
                    <li>Developing web applications and mobile web with Javascript, Typescript, ReactJS, CSS.</li>
                    <li>Leading Frontend Teams in different projects and cultures.</li>
                    <li>Looking for potential talents inside the team to help them grow.</li>
                    <li>Contributing to product definition from technial side (Frontend/ Cross).</li>
                    <li>Responsible of define frontend architecture.</li>
                    <li>Contributing to Design System definition.</li>
                    <li>Contributing to product definition from technial side (Frontend/ Cross).</li>
                    <li>Participate in mentoring program as a mentor.</li>
                    </ul>
                </i>
            </li>
        </ul>
        <h3 class="body-headline my-2">Hobbies</h3>
        <div class="row">
          <span class="hobbie">Music</span>
          <span class="hobbie">Piano</span>
          <span class="hobbie">Drums</span>
          <span class="hobbie">Games</span>
          <span class="hobbie">Family</span>
        </div>
    </div>
</div>
<style>
* {
  font-family: 'DM Sans',sans-serif;
  margin:0;
  padding:0;
}
.space-between {
  justify-content: space-between;
  align-items: center;
}
.row {
  display:flex;
  flex-direction: row;
  position:relative;
}
.hobbie {
  box-sizing:border-box;
  padding: 2rem;
  border: 2px solid #DF5B3E;
  border-radius: 50%;
  width: 60px;
  height: 60px;
  display:inline-flex;
  justify-content: center;
  align-items: center;
  font-weight: 900;
  margin: 5px;
}
.body-headline {
  box-sizing: border-box;
  color: #555;
  text-transform: uppercase;
  position:relative;
  padding-left: 28px;
}
.body-headline:before {
  content:'';
  display: inline-block;
  width: 20px;
  height:20px;
  background-image: url('./images/icon.png');
  background-size: contain;
  position: absolute;
  top: 50%;
  left:0;
  transform: translateY(-50%);
}
.body-headline:after {
  content: '';
  display: block;
  position: absolute;
  height:1px;
  background-color: #555;
  right:2rem;
  top: 50%;
  width: 170px;
  transform: translateY(-50%);
}
.progress-bar {
  position:relative;
  display: inline-block;
  border: 1px solid #333;
  height: 5px;
  width: 100px;
  border-radius: 2.5px;
}
.progress-bar .progress {
  position: absolute;
  display:inline-block;
  height:100%;
  background-color: #333;
  left:0;
  border-radius: 2.5px;
}
.slider-bar {
  position:relative;
  float:right;
  display: inline-block;
  border: 1px solid #333;
  background-color:#333;
  height: 2px;
  width: 100px;
  border-radius: 2.5px;
  top: 10px;
}
.slider-bar .slider {
  position: absolute;
  display:inline-block;
  background-color: #DF5B3E;
  border: 2px solid #333;
  height:8px;
  width: 8px;
  border-radius: 50%;
  top:-5px;
}
.percent-10{
  width:10%;
}
.percent-20{
  width:20%;
}
.percent-30{
  width:30%;
}
.percent-40{
  width:40%;
}
.percent-50{
  width:50%;
}
.percent-60{
  width:60%;
}
.percent-70{
  width:70%;
}
.percent-80{
  width:80%;
}
.percent-90{
  width:90%;
}
.percent-100{
  width:100%;
}
.percent-10-slide{
  left:10%;
}
.percent-20-slide{
  left:20%;
}
.percent-30-slide{
  left:30%;
}
.percent-40-slide{
  left:40%;
}
.percent-50-slide{
  left:50%;
}
.percent-60-slide{
  left:60%;
}
.percent-70-slide{
  left:70%;
}
.percent-80-slide{
  left:80%;
}
.percent-90-slide{
  left:90%;
}
.percent-100-slide{
  left:100%;
}
.block{
  display:block;
}
.my-1 {
  width:100%;
  margin: 1rem 0;
}
.my-2 {
  width:100%;
  margin: 2rem 0;
}
.mt-2 {
  margin-top: 2rem;
}
.mt-6{
  margin-top: 10rem;
}
.cover-image {
  margin-bottom: 1rem;
  border-radius: 50%;
}
.text-left {
  align-self: flex-start;
}
a {
  content: '';
  text-decoration: none;
  color: inherit;
  font-size: 12px;
}
.underline {
  display: block;
  border-bottom: 1px solid #333;
  padding-bottom: 5px;
  width:80%;
}
.contact-info {
  color:#DF5B3E; 
  font-weight: 700;
  text-align: left;
  box-sizing: border-box;
}
.zero-margin {
  margin: 0;
  padding: 0;
}
.flex {
  background-color: #DDD;
  color: #444;
  display:flex;
  flex-direction: column;
  min-width: 199px;
  align-items: center;
}
.white {
  background-color:white;
  color: #333;
}
h1 {
  font-weight: 900;
}
p {
  font-size:12px;
}
.inner-list{
    margin-left: 20px;
}
@media print { 
    @page { 
        margin-top: -65px; 
        margin-bottom: 0; 
        margin-right:-10px;
        margin-left:-10px;
    } 
    body { 
        padding-top: 72px; 
        padding-bottom: 72px;
        height: 100vh;
    } 
} 
</style>
